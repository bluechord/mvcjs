

//This support for Object.create ist taken from Douglas Crockford's article on
//Prototypal Inheritance(http://javascript.crockford.com/protoypal.html)
if (typeof Object.create !== "function")
    Object.create = function(o) {
        function F() {}
        F.prototype = o;
        return new F();
    };

//This pseudorandom GUID generator is written by Robert Kieffer
//(http://www.broofa.com/2008/09/javascript-uuid-function/)
Math.guid = function(){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random()*16|0, v = c === 'x' ? r : (r&0x3|0x8);
        return v.toString(16);
    }).toUpperCase();
};

if (typeof assert !== 'function') {
    assert = function(value, msg) {
        if(!value)
            throw (msg || (value + " does not equal true"));
    };
}
if (typeof assertEqual !== 'function') {
    assertEqual = function(val1, val2, msg) {
        if(val1 !== val2)
            throw (msg || (val1 + " does not equal " + val2));
    };
}

var Model = {
    prototype: {
        newRecord: true,
        
        save: function(){
            this.newRecord ? this.create() : this.update();
        },
        create: function() {
            if (!this.id) this.id = Math.guid();
            this.newRecord = false;
            this.parent.records[this.id] = this.dup();
        },
        update: function() {
            this.parent.records[this.id] = this.dup();
        },
        destroy: function(){
            delete this.parent.records[this.id];
        },
        init: function(atts){
            if (atts) this.load(atts);
        },
        load: function(atts){
            for (var name in atts)
                this[name] = atts[name];
        },
        dup: function() {
            return $.extend(true, {}, this);
        },
        attributes : function(){
            var result = {};
            for (var i in this.parent.attributes) {
                var attr = this.parent.attributes[i];
                result[attr] = this[attr];
            }
            result.id = this.id;
            return result;
        },
        toJSON: function(){
            return(this.attributes());
        }
    },
    records: {},
    
    create: function(){
        var object = Object.create(this);
        object.parent = this;
        object.prototype = object.fn = Object.create(this.prototype);
        
        object.created();
        this.inherited(object);
        return object;
    },
    init: function(){
        var instance = Object.create(this.prototype);
        instance.parent = this;
        instance.init.apply(instance, arguments);
        return instance;
    },
    extend: function( obj ){
        var extended = obj.extended;
        for (var i in obj) {
            this[i] = obj[i];
        }
        if(extended) extended(this);
    },
    include: function( obj ){
        var included = obj.included;
        for (var i in obj) {
            this.prototype[i] = obj[i];
        }
        if(included) included(this.prototype);
    },
    find: function(id) {
        if (this.records[id]) {
            var record = this.records[id];
            if (!record) throw ("Unknown record");
            return record.dup();
            
        } else throw('Unknown record');
    },
    autoPopulate : function( callback ) {
        
        var self = this;
        if (!self.readDBPath) {
            throw ('Auto populating not possible.')
            return false;
        }
        $.getJSON(self.readDBPath, function(result) {
            self.populate(result);
            if (typeof callback === 'function')
                callback();
        });
    },
    populate: function(values) {
        this.records = {};
        for ( var i=0, il = values.length; i<il;i++) {
            var record = this.init(values[i]);
            record.newRecord = false;
            this.records[record.id] = record;
        }
    },
    createRemote: function (url, callback) {
        var self = this;
        $.ajax({
            type: 'POST',
            url: url,
            success: callback,
            data: self.attributes()
        });
    },
    updateRemote: function (url, callback) {
        var self = this;
        $.ajax({
            type: 'PUT',
            url: url,
            success: callback,
            data: self.attributes()
        });
    },
    autoCreateRemote: function (callback) {
        var self = this;
        if (!self.createDBPath) {
            throw ('Auto populating not possible.')
            return false;
        }
        $.ajax({
            type: 'POST',
            url: self.createDBPath,
            success: callback,
            data: self.attributes()
        });
    },
    autoUpdateRemote: function (callback) {
        var self = this;
        if (!self.updateDBPath) {
            throw ('Auto populating not possible.')
            return false;
        }
        $.ajax({
            type: 'PUT',
            url: self.updateDBPath,
            success: callback,
            data: self.attributes()
        });
    },
    saveLocal: function(name){
        //Turn records into an array
        var result = [];
        for (var i in this.records)
            result.push(this.records[i]);

        localStorage[name] = JSON.stringify(result);
    },
    loadLocal: function(name){
        var result = JSON.parse(localStorage[name]);
        this.populate(result);
    },
            
    inherited: function(){},
    created: function(){
        this.records = {};
    }
};

//Wrapped functions like this act as modules. Its methods stay alive even 
//after the function run through! 
var exports = this;
(function($){
    
    var mod = {};
        
    mod.create = function (includes) {
        var result = function (){
            //With the Cue-Word 'new' on created Instances, theire init function will
            //be executed with given arguments:
            this.init.apply(this, arguments);
        };
        result.fn = result.prototype;
        result.fn.init = {};
        
        result.proxy = function(func) {return $.proxy(func, this);};
        result.fn.proxy = result.proxy;
        
        result.include = function(ob) {$.extend(this.fn, ob);};
        result.extend = function(ob) { $.extend(this, ob);};
        
        result.fn.$ = function(selector) {/*Scope Queries*/ return $(selector, this.view);};
        result.fn.queryElements = function() {
            //Set up local Variables from given elements-Object
            for (var key in this.elements) {
                this.elements[key] = this.$(this.elements[key]);
            }
        };
        result.fn.findElement = function(elem) {
            if(this.elements[elem]) return this.elements[elem];
            return false;
        };
    
        //Split on the first Space
        result.fn.eventSplitter = /^(\w*)\s*(.*)$/;
        result.fn.delegateEvents = function(){
            for (var key in this.events) {
                var placeholder = false;
                
                var methodName = this.events[key];
                
                var method = [];
                var type = typeof methodName;
                if(type !== "string") {
                    for(var i=0; i < methodName.length; i++) method[i] = this.proxy(this[methodName[i]]);
                } else {
                    method[0] = this.proxy(this[methodName]);
                }
            
                var match = key.match(this.eventSplitter);
                var eventName = match[1];
                var selector = match[2];
                
                if(selector.indexOf('%') === 0) {
                    selector = selector.replace('%','');
                    placeholder = true;
                }
                
                if(selector === "" || selector === "view") {
                    for (var i=0; i<method.length; i++) this.view.bind(eventName, method[i]);                    
                } else {
                    if (placeholder) {
                        if(this.findElement(selector)) {
                            for (var i=0; i<method.length; i++) this.elements[selector].bind(eventName, method[i]);
                        } else {
                            throw ("Selector is not in the Controller");
                        }
                    } else {
                        for (var i=0; i<method.length; i++) this.view.delegate(selector, eventName, method[i]);
                    }
                }
            }
        };
        if (includes) result.include(includes);
                
        return result;
    };

    exports.Controller = mod;
    
})(jQuery);
