var Class = function(){
    var klass = function() {
        this.init.apply(this, arguments);
    };
    
    //Shortcuts
    klass.fn = klass.prototype;
    klass.fn.parent = klass;
    
    //Adding Class Properties
    klass.extend = function( obj ){
        var extended = obj.extended;
        for (var i in obj) {
            klass[i] = obj[i];
        }
        if(extended) extended(klass)
    };
    
    //Adding Instance Properties
    klass.include = function( obj ){
        var included = obj.included;
        for (var i in obj) {
            klass.fn[i] = obj[i];
        }
        if(included) included(klass)
    };
    
    //Adding a proxa function
    klass.proxy = function(func) {
        var self = this;
        return (function() {
            func.apply(self, arguments);
        });
    };
    
    return klass;
};


var Copy = new Class();
Copy.extend({
    
   //--- Fields ---//
   memory: null,
   copyIndex: 0,
   
   //--- Getters and Setters ---//
   getMemory : function () {
       return this.memory;
   },
   setMemory : function ( _memory ) {
       this.memory = _memory;
   },
   getCopyIndex : function () {
       return this.copyIndex;
   },
   setCopyIndex : function ( _index ) {
       this.copyIndex = _index;
   },
   
   //--- Methods ---//
   copy : function ( obj ) {
       
       //This Code Snipet original Comes from A. Levy in an answer on stackoverflow.com
       //and was modified by me
       
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Date
        if (obj instanceof Date) {
            var copy = new Date();
            copy.setTime(obj.getTime());
            this.setMemory(copy);
        }

        // Handle Array
        if (obj instanceof Array) {
            var copy = [];
            for (var i = 0, len = obj.length; i < len; i++) {
                copy[i] = obj[i];
            }
            this.setMemory(copy);
        }

        // Handle Object
        if (obj instanceof Object) {
            var copy = {};
            $.extend(true,copy, obj);
            this.setMemory(copy);
        }

        throw new Error("Unable to copy obj! Its type isn't supported.");
   },
   shortCut : function ( obj, parent ) {
       
       //This Code Snipet original Comes from A. Levy in an answer on stackoverflow.com
       //and was modified by me
       
        // Handle the 3 simple types, and null or undefined
        if (null == obj || "object" != typeof obj) return obj;

        // Handle Object
        if (obj instanceof jQuery) {
            var copy = {};
            $.extend(true,copy, obj);
            
            //Dem kopierten Objekt eine neue id geben
            if (copy.attr('id') !== "" && copy.attr('id') !== undefined) {
                copy.attr('id', copy.attr('id') + "_copy");
            } else {
                copy.attr('id', 'copy_' + this.getCopyIndex());
                this.setCopyIndex(this.getCopyIndex() + 1);
            }
            
            //Offset kopieren:
            var offset = obj.offset();
            var box = {
                width : obj.width(),
                height : obj.height()
            };
            
            //Dem neuen Parent zuweisen:
            if (parent instanceof jQuery) parent.append(copy);
            else $(parent).append(copy);
            
            //Offset zuweisen:
            copy.css({width : box.width, height : box.height});
            copy.offset({left : offset.left, top: offset.top});
            
            //Sichtbarkeit umschalten:
            obj.css('display','none');
            copy.css('display','block');
            
            return copy;
        }
        
        throw new Error("Unable to copy obj! Its type isn't supported.");
   },
   paste : function ( parent ) {
       if (parent instanceof jQuery) parent.append(this.getMemory());
       else $(parent).append(this.getMemory());
   }
   
   //--- Constructor ---//
});

